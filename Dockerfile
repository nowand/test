FROM docker-registry.default.svc:5000/proxy2/nginx-base:latest

ADD rootCA.crt /etc/pki/ca-trust/source/anchors/

USER root

RUN update-ca-trust

USER 1001
